<?php

require('../vendor/autoload.php');

$app = new Silex\Application();
$app['debug'] = true;

// Register the monolog logging service
$app->register(new Silex\Provider\MonologServiceProvider(), array(
  'monolog.logfile' => 'php://stderr',
));

// Our web handlers

$app->get('/', function() use($app) {
  $app['monolog']->addDebug('logging output.');
  return '<html>
          <body>
		  <ul>
		  <li><a href= "https://sleepy-temple-7368.herokuapp.com/Project2-Map1.html">Google map1</a></li>
		  <li><a href= "https://sleepy-temple-7368.herokuapp.com/Project2-Map2.html">Google map2</a></li>
		  <li><a href= "https://sleepy-temple-7368.herokuapp.com/Project2-Map3.html">Google map3</a></li>
		  <li><a href= "https://sleepy-temple-7368.herokuapp.com/Project2-Map4.html">Google map4</a></li>
		  </body> 
		  </html>';
});

$app->run();

?>
